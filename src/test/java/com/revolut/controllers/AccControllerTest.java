package com.revolut.controllers;

import com.revolut.RevolutApp;
import io.restassured.response.Response;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;
import static org.junit.Assert.assertEquals;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class AccControllerTest {

    public static final String ACCOUNTS_ENDPOINT = "/accounts";


    @Before
    public void setUp() {
        RevolutApp.startRevolutApp();

    }

    @After
    public void tearDown() {
        RevolutApp.stopApp();
    }

    @Test
    public void create_account_through_http_endpoint_expect_success_201_status() {

        given().
                when().
                body("{name:vladi}").
                post(ACCOUNTS_ENDPOINT).
                then().
                assertThat().statusCode(201);

    }

    @Test
    public void get_account_by_id_through_http_endpoint_expect_404_status() {

        String id = "1";
        Response res = post(ACCOUNTS_ENDPOINT + "/" + id);

        assertEquals(res.statusCode(), 404);
    }

}
