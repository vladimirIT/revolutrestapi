package com.revolut.service;

import com.revolut.domain.AccountEntity;
import com.revolut.model.EndpointOperationResponsePayload;
import com.revolut.repository.AccountEntityRepository;
import com.revolut.repository.AccountTransactionRepository;
import com.revolut.util.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class AccountServiceTest {

    @Mock
    private AccountEntityRepository accountEntityRepository;
    @Mock
    private AccountTransactionRepository accountTransactionRepository;
    @Mock
    private JsonParser jsonParser;
    private AccountService accountService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        accountService = new AccountService(accountEntityRepository, accountTransactionRepository, jsonParser);
    }

    @Test
    public void delete_account_expect_status_code_400() {

        long id = 3;
        AccountEntity accountEntity = new AccountEntity(id);
        accountEntity.setName("ff");

        Mockito.when(accountEntityRepository.getAccountById(id)).thenReturn(accountEntity);
        EndpointOperationResponsePayload endpointOperationResponsePayload = accountService.deleteAccountById(String.valueOf(id));

        assertEquals(endpointOperationResponsePayload.getStatusCode(), 204);
    }
}
