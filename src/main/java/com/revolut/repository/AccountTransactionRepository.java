package com.revolut.repository;

import com.revolut.domain.AccountTransactionEntity;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

import java.util.List;

public interface AccountTransactionRepository {

    void createAccountTransaction(AccountTransactionEntity accountTransactionEntity) throws Exception;
    List<AccountTransactionEntity> getAccountTransactionsByAccountId(Long accountId);

}
