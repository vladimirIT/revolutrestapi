package com.revolut.repository;

import com.revolut.domain.AccountTransactionEntity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Date;
import java.util.List;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class AccountTransactionRepositoryDefaultImpl implements AccountTransactionRepository {

    private EntityManager entityManager;

    @Inject
    public AccountTransactionRepositoryDefaultImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void createAccountTransaction(AccountTransactionEntity accountTransactionEntity) throws Exception {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(accountTransactionEntity);
            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
    }

    @Override
    public List<AccountTransactionEntity> getAccountTransactionsByAccountId(Long accountId) {
        Query query = entityManager.createQuery("from " + AccountTransactionEntity.class.getName() + " a");

        List<AccountTransactionEntity> accountTransactionEntities = query.getResultList();

        accountTransactionEntities.forEach(accountTransactionEntity -> System.out.println("\ntransaction entity = " + accountTransactionEntity.toString()));
        return accountTransactionEntities;
    }
}
