package com.revolut.domain;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

@Entity(name = "account ")
@Table(name = "account ")
public class AccountEntity {

    @Id
    @GeneratedValue
    private long id;

    @NotEmpty(message = "Name must be present")
    @Column(name = "name")
    private String name;

    @Column(name = "account_balance")
    private BigDecimal accountBalance;

    public AccountEntity() {

    }

    public AccountEntity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public AccountEntity setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public AccountEntity setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
        return this;
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }
}
