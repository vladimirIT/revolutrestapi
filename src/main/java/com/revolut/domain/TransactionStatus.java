package com.revolut.domain;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public enum TransactionStatus {
    FAILED, SUCCESS
}
