package com.revolut.util;

import com.revolut.model.EndpointOperationResponsePayload;
import spark.Response;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public interface ResponseCreator {
    String respondToHttpEndpoint(Response response, EndpointOperationResponsePayload endpointOperationResponsePayload);
}
