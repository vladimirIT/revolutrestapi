package com.revolut.util;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public interface JsonParser {

    <T> T toJsonPOJO(String jsonString, Class<T> classType);
    String toJSONString(Object data);
}
