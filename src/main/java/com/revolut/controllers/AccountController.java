package com.revolut.controllers;

import spark.Request;
import spark.Response;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public interface AccountController {

    String createAccount(Request request, Response response);

    String getAllAccounts(Request request, Response response);

    String getAcountById(Request request, Response response);

    String deleteAccountById(Request request, Response response);

    String createAccountTransaction(Request request, Response response) throws Exception;

    String getAllTransactionsOfAccount(Request request, Response response);
}
