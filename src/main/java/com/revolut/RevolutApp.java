package com.revolut;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.controllers.AccountController;
import com.revolut.module.AppModule;
import spark.Filter;
import spark.Spark;

import java.util.HashMap;

import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.stop;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class RevolutApp {
    public static final int MAIN_PORT = 8080;

    public static void main(String[] args) {
        setupPortNumber();
        applyCORSFilter();
        initializeRoutes();
    }

    public static void initializeRoutes() {
        Injector injector = Guice.createInjector(new AppModule());
        AccountController accountController = injector.getInstance(AccountController.class);

        post("/accounts", accountController::createAccount);
        get("/accounts", accountController::getAllAccounts);
        get("/accounts/:id", accountController::getAcountById);
        delete("/accounts/:id", accountController::deleteAccountById);
        post("/accounts/:id/transactions", accountController::createAccountTransaction);
        get("/accounts/:id/transactions", accountController::getAllTransactionsOfAccount);
    }

    public static void setupPortNumber() {
        port(MAIN_PORT);
    }

    public static void startRevolutApp() {
        RevolutApp.main(null);
        awaitInitialization();
    }

    public static void stopApp() {
        stop();
        awaitStop();
    }

    private static final HashMap<String, String> corsHeaders = new HashMap<>();

    static {
        corsHeaders.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
        corsHeaders.put("Access-Control-Allow-Origin", "*");
        corsHeaders.put("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
        corsHeaders.put("Access-Control-Allow-Credentials", "true");
    }

    public final static void applyCORSFilter() {
        Filter filter = (request, response) -> corsHeaders.forEach((key, value) -> response.header(key, value));
        Spark.after(filter);
    }
}
