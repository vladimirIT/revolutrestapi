package com.revolut.model;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class EndpointOperationResponsePayload {
    private int statusCode;
    private String endpointResponseBody;

    private String errorReason;

    public EndpointOperationResponsePayload(int statusCode, String endpointResponseBody) {
        this.statusCode = statusCode;
        this.endpointResponseBody = endpointResponseBody;
    }

    public EndpointOperationResponsePayload(int statusCode, String endpointResponseBody, String errorReason) {
        this.statusCode = statusCode;
        this.endpointResponseBody = endpointResponseBody;
        this.errorReason = errorReason;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public EndpointOperationResponsePayload setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    public String getEndpointResponseBody() {
        return endpointResponseBody;
    }

    public EndpointOperationResponsePayload setEndpointResponseBody(String endpointResponseBody) {
        this.endpointResponseBody = endpointResponseBody;
        return this;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public EndpointOperationResponsePayload setErrorReason(String errorReason) {
        this.errorReason = errorReason;
        return this;
    }
}