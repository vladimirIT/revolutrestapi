package com.revolut.model;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class SuccessfulOperationWithEmptyBodyPayload  extends EndpointOperationResponsePayload{
    public SuccessfulOperationWithEmptyBodyPayload(int statusCode) {
        super(statusCode, "", "");
    }
}
