package com.revolut.model;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class SuccessfulOperationWithJSONBodyResponsePayload extends EndpointOperationResponsePayload {

    public SuccessfulOperationWithJSONBodyResponsePayload(int statusCode, String endpointResponseBody) {
        super(statusCode, endpointResponseBody, "");
    }
}
