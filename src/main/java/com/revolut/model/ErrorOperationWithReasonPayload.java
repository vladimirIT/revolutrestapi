package com.revolut.model;

/**
 * Revolut App for Money Transfer
 * Developed by Vladimir Stratiev
 */

public class ErrorOperationWithReasonPayload extends EndpointOperationResponsePayload {
    public ErrorOperationWithReasonPayload(int statusCode) {
        super(statusCode, "");
    }
}
