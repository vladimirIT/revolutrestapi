# Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts.<br>

# Run Application
```
1) ./gradlew build
2) ./gradlew startRevolutApp 
```
#The application will start on port 8080
# Run All Tests
```$xslt
./gradlew test
```
# Create an account
```
    POST localhost:8080/api/accounts
    { 
        "name":"vladimir", 
        "accountBalance":1000 
    }
```
# Get All Accounts 
```
    GET http://localhost:8080/accounts
    
```
Response:
```
    HTTP 200 OK
[
    {
        "id": 1,
        "name": "vladi",
        "accountBalance": 2500000
    },
    {
        "id": 2,
        "name": "benas",
        "accountBalance": 10000000
    }
]
```
### Money Transfer Transaction
Transfer money from one account to another:
```
    POST http://localhost:8080/accounts/1/transactions
    
    { 
    "sendingAccountId":1, 
    "receivingAccountId":2, 
    "transactionAmount":15000
    }
```
#Result after transaction should be 
```
    POST http://localhost:8080/accounts/1/transactions
    
    { 
    "sendingAccountId":1, 
    "receivingAccountId":2, 
    "transactionAmount":15000
    }
```
# Retrieve all money transfer transactions on specific account id
The folowing request retrieves all money transfer transactions on specific account id
```
    GET http://localhost:8080/accounts/1/transactions
```
#Response:
```
    HTTP 200 OK
    [
      {
        "id": 1,
        "sendingAccountId": 1,
        "receivingAccountId": 2,
        "transactionAmount": 5000,
      },
      {
        "id": 2,
        "sendingAccountId": 3,
        "receivingAccountId": 4,
        "transactionAmount": 100000,
      }
    ]

```